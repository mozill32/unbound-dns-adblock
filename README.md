Unbound DNS configuration files for blocking ads (Please view in RAW format)


Regex used to generate these configuration files from source. Can be used in Notepad++


Use this to apend onto the tld's
Find: (\.[A-Z]{2,8})($| )
Replace: $1 A 0.0.0.0"

Use this to replace beginning
Find: ^127.0.0.1

or

Find: ^0.0.0.0

Replace: local-data: "